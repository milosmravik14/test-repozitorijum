function showCart() {
    fetch(BASE + 'api/carts', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            displayCart(data.carts);
        });
}

function addToCart(productId) {
    fetch(BASE + 'api/carts/add/' + productId, { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                showCart();
            }
        });
}

function clearCart() {
    fetch(BASE + 'api/carts/clear', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                showCart();
            }
        });
}

function enableDisableButton(carts) {
    document.querySelector("#checkout").href = BASE + 'cart';
    document.querySelector("#checkout").classList.remove('disabled');

    if (carts.length == 0) {
        console.log('prazna je');
        document.querySelector('.cart').innerHTML = 'Korpa je trenutno prazna!';
        document.querySelector("#checkout").href = '#';
        document.querySelector("#checkout").classList.add('disabled');
        
        return;
    }
}

function displayCart(carts) {
    const cartsDiv = document.querySelector('.cart');
    cartsDiv.innerHTML = '';

    enableDisableButton(carts);

    for (cart of carts) {
        const cartLink = document.createElement('a');
        cartLink.style.display = 'block';
        cartLink.innerHTML = cart.name;
        cartLink.href = BASE + 'product/' + cart.product_id;

        cartsDiv.appendChild(cartLink);
    }
}

addEventListener('load', showCart);
