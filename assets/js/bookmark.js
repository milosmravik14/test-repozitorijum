function getBookmarks() {
    fetch(BASE + 'api/bookmarks', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            console.log(data);
            displayBookmarks(data.bookmarks);
        });
}

function addBookmark(productId){
    fetch(BASE + 'api/bookmarks/add/' + productId, { credentials: 'include' })
        .then(result => result.json())    //json.parse(text, value)?
        .then(data => {
            console.log(data);
            if(data.error === 0){
                getBookmarks();
            }
    });
}

function clearBookmarks() {
    fetch(BASE + 'api/bookmarks/clear', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if(data.error === 0){
                getBookmarks();

            }
    });
}

function displayBookmarks(bookmarks){
    const bookmarksDiv = document.querySelector('.bookmarks');
    bookmarksDiv.innerHTML = '';

    if(bookmarks.lenght == 0){
        bookmarksDiv.innerHTML = 'Shopping cart is empty!';
        return;

    }

    for(bookmark of bookmarks){
        const bookmarkLink = document.createElement('a');
        bookmarkLink.style.display = 'block';
        bookmarkLink.innerHTML = bookmark.title;
        bookmarkLink.href = BASE + 'product/' + bookmark.product_id;

        bookmarksDiv.appendChild(bookmarkLink);

    }
    console.log(bookmarks);
}

addEventListener('load', getBookmarks);