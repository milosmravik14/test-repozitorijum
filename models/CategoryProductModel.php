<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CategoryProductModel extends Model {
        protected function getFields(): array {
            return [
                'category_product_id'     => new Field( (new NumberValidator())->setIntegerLength(11), false),
                'category_id'             => new Field( (new NumberValidator())->setIntegerLength(11)),
                'product_id'              => new Field( (new NumberValidator())->setIntegerLength(11)),
            ];
        }
        public function getAllBySql(Type $var = null)
        {
            # code...
        }
    }