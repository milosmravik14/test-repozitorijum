<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ImageModel extends Model {

        protected function getFields(): array {
            return [
                'image_id'        => new Field((new NumberValidator())->setIntegerLength(11), false),
                'title'           => new Field((new StringValidator())->setMaxLength(255)),
                'path'            => new Field((new StringValidator())->setMaxLength(255)),
                'product_id'      => new Field((new NumberValidator())->setIntegerLength(11), false)
            ];
        }

        public function getAllByProductId($productId) {
            return $this->getAllByFieldName('product_id', $productId);
        }

        public function getAllByCatalogueId($catalogueId) {
            return $this->getAllByFieldName('catalogueId', $catalogueId);
        }

    }