<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class OfferModel extends Model {

        protected function getFields(): array {
            return [
                'offer_id'        => new Field((new NumberValidator())->setIntegerLength(20), false),
                'created_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                'user_id'         => new Field((new NumberValidator())->setIntegerLength(11)),
                'status'          => new Field((new StringValidator())->setMaxLength(10)),                
                'currency_id'     => new Field((new NumberValidator())->setIntegerLength(11)),
                'shipping_id'     => new Field((new NumberValidator())->setIntegerLength(11))
            ];
        }

        public function getAllOffers(): array{       //ZAVRSITI OVAJ DEO!!!!!! 30 SNIMAK 30MIN                  
            $sql = 'SELECT * FROM offer;';
            $prep = $this->getConnection()->prepare($sql);     //Call a member function on null
            $res = $prep->execute();

            $offers = [];
            if($res){
                $offers = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $offers;
        }

        public function getOfferByUserId(int $id){
            return $this->getByFieldName('user_id', $id);

        }


    }