<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ExchangeRateModel extends Model {
        protected function getFields(): array {
            return [
                'exchange_rate_id' => new Field( (new NumberValidator())->setIntegerLength(11), false),
                'date'             => new Field( (new DateTimeValidator())->allowDate()),
                'value'            => new Field((new NumberValidator())->setUnsigned()
                                                                      ->setIntegerLength(7)
                                                                      ->setMaxDecimalDigits(2)),
                'currency_id'      => new Field( (new NumberValidator())->setIntegerLength(11))
            ];
        }

    }