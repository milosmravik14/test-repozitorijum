<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ProductModel extends Model {
        protected function getFields(): array {
            return [
                'product_id'      => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'product_no'      => new Field((new NumberValidator())->setIntegerLength(11)),
                'title'           => new Field((new StringValidator())->setMaxLength(255)),
                'description'     => new Field((new StringValidator())->setMaxLength(64*1024)),
                'price'           => new Field((new NumberValidator())#->setDecimal()
                                                                       ->setUnsigned()
                                                                       ->setIntegerLength(10)
                                                                       ->setMaxDecimalDigits(2)),
                'producer'        => new Field( (new StringValidator())->setMaxLength(255)),
                'size'            => new Field ((new NumberValidator())#->setDecimal()
                                                                       ->setIntegerLength(62)
                                                                       ->setMaxDecimalDigits(2)),
                'weight'          => new Field ((new NumberValidator())#->setDecimal()
                                                                       ->setIntegerLength(62)
                                                                       ->setMaxDecimalDigits(2)),
                'modified_at'     => new Field((new DateTimeValidator())->allowDate()->allowTime()),
                'is_active'       => new Field(new BitValidator()),
                'image_path'      => new Field((new StringValidator())->setMaxLength(255)),
            ];
        }

        public function fillProductImages(&$product) {
            $db = $this->getDatabaseConnection();
            $product->images = (new \App\Models\ImageModel($db))->getAllByProductId($product->product_id);
            return $product;
        }

        public function getAllByCategoryId(int $categoryId): array{       //ZAVRSITI OVAJ DEO!!!!!! 30 SNIMAK 30MIN                  
            $sql = 'SELECT * FROM product WHERE product_id IN (SELECT product_id FROM category_product WHERE category_id = ?);';
            $prep = $this->getConnection()->prepare($sql);     //Call a member function on null
            $res = $prep->execute([$categoryId]);

            $products = [];
            if($res){
                $products = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return array_map([$this, 'fillProductImages'], $products);
        }

        public function getAllByCatalogueId(int $catalogueId): array{                   
            $sql = 'SELECT * FROM product WHERE product_id IN (SELECT product_id FROM category_product WHERE category_id IN (SELECT category_id FROM category WHERE catalogue_id = ?));';
            $prep = $this->getConnection()->prepare($sql);     
            $res = $prep->execute([$catalogueId]);

            $products = [];
            if($res){
                $products = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            
            return array_map([$this, 'fillProductImages'], $products);
        }

        public function getAllByOfferId(int $offerId): array{                   
            $sql = 'SELECT * FROM product WHERE product_id IN (SELECT product_id FROM product_offer WHERE offer_id = ?);';
            $prep = $this->getConnection()->prepare($sql);     
            $res = $prep->execute([$offerId]);

            $products = [];
            if($res){
                $products = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            
            return array_map([$this, 'fillProductImages'], $products);
        }

        public function searchProducts(string $productTitle): array{                   
            $sql = 'SELECT * FROM product WHERE title LIKE = ? );';
            $prep = $this->getConnection()->prepare($sql);     
            $res = $prep->execute(['%' . $productTitle . '%']);

            $products = [];
            if($res){
                $products = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            
            return array_map([$this, 'fillProductImages'], $products);
        }

        public function getAllBySearch(string $keywords) {
            $sql = 'SELECT * FROM product WHERE title LIKE ? OR description LIKE ?;';
            $keywords= '%' . $keywords . "%";
            $prep = $this->getConnection()->prepare($sql);
            if (!$prep) {
                return [];
            }

            $res = $prep->execute([$keywords, $keywords]);

            if($res) {
                $products = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return array_map([$this, 'fillProductImages'], $products);
        }

        public function getAllByFilter(int $categoryId, string $producer, int $price, int $price_to) {
            $sql =  'SELECT * FROM category INNER JOIN category_product ON category.category_id = category_product.category_id INNER JOIN product ON category_product.product_id = product.product_id  WHERE category.category_id = ? AND product.producer = ? AND product.price BETWEEN ? AND ? ORDER BY product.price ASC;';   
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$categoryId, $producer, $price, $price_to]);

            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        
        public function addToCart(){

            
        }

    }