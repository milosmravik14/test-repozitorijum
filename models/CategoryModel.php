<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CategoryModel extends Model {
        protected function getFields(): array {
            return [
                'category_id'     => new Field( (new NumberValidator())->setIntegerLength(11), false),
                'title'           => new Field((new StringValidator())->setMaxLength(255)),
                'image_path'      => new Field((new StringValidator())->setMaxLength(255)),
                'catalogue_id'    => new Field( (new NumberValidator())->setIntegerLength(11)),            #stavljeno editable zbog dodavanja i editovanja kategorija
                'is_active'       => new Field(  new BitValidator())
            ];
        }
        public function getAllBySql(Type $var = null)
        {
            # code...
        }
    }