<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ShippingModel extends Model {
        protected function getFields(): array {
            return [
                'shipping_id'      => new Field((new NumberValidator())->setIntegerLength(11), false),
                'title'           => new Field( (new StringValidator())->setMaxLength(255)),
                'price'           => new Field ((new NumberValidator())->setDecimal()),
                'admin_id'        => new Field((new NumberValidator())->setIntegerLength(11), false)
            ];
        }

    }
