<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class SpecialOfferModel extends Model {
        protected function getFields(): array {
            return [
                'special_offer_id' => new Field( (new NumberValidator())->setIntegerLength(11), false),
                'discount'         => new Field((new NumberValidator())->setUnsigned()
                                                                       ->setIntegerLength(2)
                                                                       ->setMaxDecimalDigits(2)),
                'date_from'        => new Field( (new DateTimeValidator())->allowDate()),
                'date_to'          => new Field( (new DateTimeValidator())->allowDate()),
                'product_id'       => new Field( (new NumberValidator())->setIntegerLength(11))
            ];
        }

    }