<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CatalogueModel extends Model {

        protected function getFields(): array {
            return [
                'catalogue_id'    => new Field( (new NumberValidator())->setIntegerLength(11), false),
                'title'           => new Field( (new StringValidator())->setMaxLength(255)),
                'description'     => new Field( (new StringValidator())->setMaxLength(64*1024))
            ];
        }

        public function getAllCatalogues(): array{       //ZAVRSITI OVAJ DEO!!!!!! 30 SNIMAK 30MIN                  
            $sql = 'SELECT * FROM catalogues;';
            $prep = $this->getConnection()->prepare($sql);     //Call a member function on null
            $res = $prep->execute();

            $catalogues = [];
            if($res){
                $catalogues = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $catalogues;
        }

    }