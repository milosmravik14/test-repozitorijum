<?php
    return [

        //Register
        \App\Core\Route::get ('|^user/register/?$|',                 'Main',                      'getRegister'),  //prikaz metoda
        \App\Core\Route::post('|^user/register/?$|',                 'Main',                      'postRegister'),  //obrada podataka
        
        //Login    
        \App\Core\Route::get ('|^user/login/?$|',                    'Main',                      'getLogin'),  //prikaz metoda
        \App\Core\Route::post('|^user/login/?$|',                    'Main',                      'postLogin'),

        //Logout
        \App\Core\Route::get ('|^user/logout/?$|',                   'Main',                      'getLogout'),  //prikaz metoda
        \App\Core\Route::post('|^user/logout/?$|',                   'Main',                      'postLogout'),

        //Category
        \App\Core\Route::get ('|^category/([0-9]+)/delete/?$|',      'Category',                  'delete'),
        \App\Core\Route::get ('|^category/([0-9]+)/?$|',             'Category',                  'show'),
        \App\Core\Route::get ('|^categories-view/?$|',               'Category',                  'view'),

        //Product
        \App\Core\Route::get('|^product/([0-9]+)/?$|',               'Product',                   'show'),
        \App\Core\Route::get('|^product/([0-9]+)/delete/?$|',        'Product',                   'delete'),
        \App\Core\Route::post('|^product-search/?$|',                'Product',                   'searchProducts'),
        
        \App\Core\Route::get('|^product-cart/?$|',                   'ApiCart',                   'addToCart'),

        //Catalogue
        \App\Core\Route::get('|^catalogue-products/([0-9]+)/?$|',    'Catalogue',                 'showProducts'),
        \App\Core\Route::get('|^catalogue-show/?$|',                 'Catalogue',                 'showCatalogues'),

        //Offer
        \App\Core\Route::get('|^offer-show/?$|',                     'Offer',                     'showOffers'),
        \App\Core\Route::get('|^offer-invoices/([0-9]+)/?$|',        'Offer',                     'showInvoices'),

        \App\Core\Route::get('|^handle/([a-z]+)/?$|',                'EventHandler',              'handle'),

        App\Core\Route::post('|^filter/?$|',                         'Product',                   'postFilter'),

        #API rute  ne odgovaraju HTML-om 
        \App\Core\Route::get('|^api/product/([0-9]+)/?$|',           'ApiProduct',                'show'),
        \App\Core\Route::get('|^api/bookmarks/?$|',                  'ApiBookmark',               'getBookmarks'),
        \App\Core\Route::get('|^api/bookmarks/add/([0-9]+)/?$|',     'ApiBookmark',               'addBookmarks'),
        \App\Core\Route::get('|^api/bookmarks/clear/?$|',            'ApiBookmark',               'clear'),

        #User role routes:
        App\Core\Route::get('|^user/profile/?$|',                    'UserDashboard',             'index'),

        #Admin role routes:
        #App\Core\Route::get('|^admin/profile/?$|',                   'AdminDashboard',            'index'),

        #Admin < Categories
        App\Core\Route::get('|^admin/categories/?$|',                'AdminCategoryManagement',   'categories'),
        App\Core\Route::get('|^admin/categories/edit/([0-9]+)/?$|',  'AdminCategoryManagement',   'getEdit'),
        App\Core\Route::post('|^admin/categories/edit/([0-9]+)/?$|', 'AdminCategoryManagement',   'postEdit'),
        App\Core\Route::get('|^admin/categories/add/?$|',            'AdminCategoryManagement',   'getAdd'),
        App\Core\Route::post('|^admin/categories/add/?$|',           'AdminCategoryManagement',   'postAdd'),

        #Admin < Products
        App\Core\Route::get('|^admin/products/?$|',                  'AdminProductManagement',    'products'),
        App\Core\Route::get('|^admin/products/edit/([0-9]+)/?$|',    'AdminProductManagement',    'getEdit'),
        App\Core\Route::post('|^admin/products/edit/([0-9]+)/?$|',   'AdminProductManagement',    'postEdit'),
        App\Core\Route::get('|^admin/products/add/?$|',              'AdminProductManagement',    'getAdd'),
        App\Core\Route::post('|^admin/products/add/?$|',             'AdminProductManagement',    'postAdd'),

        #Admin < Users
        App\Core\Route::get('|^admin/users/?$|',                      'AdminUserManagement',      'users'),
        App\Core\Route::get('|^admin/users/edit/([0-9]+)/?$|',        'AdminUserManagement',      'getEdit'),
        App\Core\Route::post('|^admin/users/edit/([0-9]+)/?$|',       'AdminUserManagement',      'postEdit'),
        App\Core\Route::get('|^admin/users/add/?$|',                  'AdminUserManagement',      'getAdd'),
        App\Core\Route::post('|^admin/users/add/?$|',                 'AdminUserManagement',      'postAdd'),

        #Admin < Catalogue
        App\Core\Route::get('|^admin/catalogues/?$|',                 'AdminCatalogueManagement', 'catalogues'),
        App\Core\Route::get('|^admin/catalogues/edit/([0-9]+)/?$|',   'AdminCatalogueManagement', 'getEdit'),
        App\Core\Route::post('|^admin/catalogues/edit/([0-9]+)/?$|',  'AdminCatalogueManagement', 'postEdit'),
        App\Core\Route::get('|^admin/catalogues/add/?$|',             'AdminCatalogueManagement', 'getAdd'),
        App\Core\Route::post('|^admin/catalogues/add/?$|',            'AdminCatalogueManagement', 'postAdd'),

        #Admin < Offers
        App\Core\Route::get('|^admin/offers/?$|',                     'AdminOfferManagement',     'offers'),
        App\Core\Route::get('|^admin/offers/edit/([0-9]+)/?$|',       'AdminOfferManagement',     'getEdit'),
        App\Core\Route::post('|^admin/offers/edit/([0-9]+)/?$|',      'AdminOfferManagement',     'postEdit'),
        App\Core\Route::get('|^admin/offers/add/?$|',                 'AdminOfferManagement',     'getAdd'),
        App\Core\Route::post('|^admin/offers/add/?$|',                'AdminOfferManagement',     'postAdd'),
        
        
        \App\Core\Route::any('|^.*$|',                                 'Main',                    'home')
    ];