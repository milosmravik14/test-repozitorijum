<?php
    namespace App\Controllers;

    class UserController extends \App\Core\Controller{   //grupisu celine na logican nacin, iz kontrolera ne smemo da pristupamo bazi podataka
        
        public function show($id) {
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($id);

            if(!$user){
                header('Location: /turbotech');
                exit;
            }

            $this->set('user', $user);



    }

}