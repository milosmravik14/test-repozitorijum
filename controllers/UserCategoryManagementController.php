<?php
    namespace App\Controllers;

    class UserCategoryManagementController extends \App\Core\Role\UserRoleController {
        public function categories() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getEdit($categoryId){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($categoryId);

            if(!$category){
                $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('category', $category);

            return $categoryModel;
        }

        public function postEdit($categoryId){
            $categoryModel = $this->getEdit($categoryId);

            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $imagePath = \filter_input(INPUT_POST, 'image_path', FILTER_SANITIZE_STRING);
            $catalogueId = \filter_input(INPUT_POST, 'catalogue_id', FILTER_SANITIZE_NUMBER_INT);

            $categoryModel->editById($categoryId, [
                'title' => $title,
                'image_path' => $imagePath,
                'catalogue_id' => $catalogueId
            ]);

            $this->redirect(\Configuration::BASE . 'user/categories');
        }

        public function getAdd(){


        }

        public function postAdd(){
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $imagePath = \filter_input(INPUT_POST, 'image_path', FILTER_SANITIZE_STRING);
            $catalogueId = \filter_input(INPUT_POST, 'catalogue_id', FILTER_SANITIZE_NUMBER_INT);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());

            $categoryId = $categoryModel->add([
                'title' => $title,
                'image_path' => $imagePath,
                'catalogue_id' => $catalogueId
            ]);

            if($categoryId){
                $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovu kategoriju!');

        }
    }