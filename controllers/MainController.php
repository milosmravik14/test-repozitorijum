<?php
    namespace App\Controllers;

    class MainController extends \App\Core\Controller{   //grupisu celine na logican nacin, iz kontrolera ne smemo da pristupamo bazi podataka
        
        public function home() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);

            }

            public function getRegister() {
                
            }

            public function postRegister() {
                $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
                $forename   = \filter_input(INPUT_POST, 'reg_forename', FILTER_SANITIZE_STRING);
                $surname    = \filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
                $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
                $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
                $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

                if($password1 !== $password2){
                    $this->set('message','Doslo je do greske: Niste uneli dva puta istu lozinku.');
                    return;
                }

                $validanPassword = (new \App\Validators\StringValidator())
                                    ->setMinLength(8)
                                    ->setMaxLength(128)
                                    ->isValid($password1);

                if ( !$validanPassword ) {
                $this->set('message','Doslo je do greske: Lozinka nije ispravnog formata.');
                return;
                }
                
                $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
                $user = $userModel->getByFieldName('email', $email);
                if($user){
                    $this->set('message', 'Doslo je do greske: vec postoji korisnik sa tom email adresom.');
                    return;
                }

                $user = $userModel->getByFieldName('username', $username);
                if($user){
                    $this->set('message', 'Doslo je do greske: vec postoji korisnik sa tim korisnickim imenom.');
                    return;
                }

                $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

                $userId = $userModel->add([
                    'username'          => $username,
                    'email'             => $email,
                    'password_hash'     => $passwordHash,
                    'forename'          => $forename,
                    'surname'           => $surname
                ]);

                /*address dooodati!!!
                phone*/  

                if(!$userId){
                    $this->set('message', 'Doslo je do greske: nije bilo uspesno registrovanje naloga.');
                    return;
                }


                $this->set('message', 'Napravljen je novi nalog! Sada mozete da se prijavite');

                $this->notifyUser($user, $userId);


            }

            public function getLogin() {


            }

            public function postLogin() {
                $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
                $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);
                
                $validanPassword = (new \App\Validators\StringValidator())
                                    ->setMinLength(7)
                                    ->setMaxLength(120)
                                    ->isValid($password);

                if ( !$validanPassword ) {
                    $this->set('message','Doslo je do greske: Lozinka nije ispravnog formata.');
                    return;
                }

                $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
                $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());
                
                $passwordHash = \password_hash($password, PASSWORD_DEFAULT);

                $user = $userModel->getByFieldName('username', $username);
                $admin =  $adminModel->getByFieldName('username', $username);

                if(!$user && !$admin) {
                    $this->set('message', 'Doslo je do greske: ne postoji korisnik sa tim username-om.');
                    return;
                }

                $zaProveru = $admin;
                if ($user) {
                    $zaProveru = $user;
                }

                if(!password_verify($password, $zaProveru->password_hash)){
                    sleep(1);
                    $this->set('message', 'Doslo je do greske: lozinka nije ispravna.');
                    return;

                }

                $this->getSession()->put('user_id', @$user->user_id);
                $this->getSession()->put('admin_id', @$admin->admin_id);
                $this->getSession()->save();

                if($user){
                    $this->redirect(\Configuration::BASE . 'user/profile');
                }

                $this->redirect(\Configuration::BASE . 'admin/profile');
            }

            public final function end(){
                session_start();
                $this->getSession()->clear();
                \session_destroy();

            }

            public function getLogout(){
                $endSession = $this->end();
               // $this->getSession()->endSession;


            }

            private function notifyUser(&$user, int $userId) {
                $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
                $user = $userModel->getById($userId);

                $html = '<!doctype html><head><meta charset="utf-8"></head><body>';
                $html .= 'Uspesno ste se registrovali na nas sajt pod username-om: ';
                $html .= \htmlspecialchars($user->username);
                $html .= ' Vase ime je: ';
                $html .= \htmlspecialchars($user->forename);
                $html .= ' Vase prezime je: ';
                $html .= \htmlspecialchars($user->surname);
                $html .= '</body></html>';

                #$mailer->Body = $html;
                #$mailer->Subject = 'Nova Registracija';
                #$mailer->addAddress($user->email);

                #$mailer->send();

                $event = new \App\EventHandlers\EmailEventHandler();
                $event->setSubject('Nova Registracija');
                $event->setBody($html);
                $event->addAddress($user->email);
                
                $eventModel = new \App\Models\EventModel($this->getDatabaseConnection());
                $eventModel->add([
                    'type' => 'email',
                    'data' => $event->getData()
                ]);

            }

    }