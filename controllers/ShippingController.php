<?php
    namespace App\Controllers;

    class ShippingController extends \App\Core\Controller{ 
        
            public function show($id) {
    
                $shippingModel = new \App\Models\ProductModel($this->getDatabaseConnection());
                $shipping = $shippingModel->getById($id);
                
                if(!$shipping){
                    header('Location: /turbotech');
                    exit;
                }
    
                $this->set('shipping', $shipping);
    
                /*$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
                echo($ipAddress);
                $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
                echo($userAgent);*/
    
               /* $productViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());
                $productViewModel->add(
                    [
                        'product_id' => $id,
                        'ip_address' => $ipAddress,
                        'user_agent' => $userAgent
                    ]
                    );*/
    
            }

    }