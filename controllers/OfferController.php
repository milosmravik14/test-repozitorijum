<?php
    namespace App\Controllers;

    class OfferController extends \App\Core\Controller {


        public function showInvoices($id) {
            $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
            $offer = $offerModel->getById($id);

            if(!$offer){
                header('Location: /turbotech');
                exit;
            }

            $this->set('offer', $offer);
            
            $this->set('products', (new \App\Models\ProductModel($this->getDatabaseConnection()))->getAllByOfferId($id));

        }

        public function showOffers() {
            $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
            $offers = $offerModel->getAll();

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $offers = array_map(function($offer) use ($userModel){
                $offer->user = $userModel->getById($offer->user_id);
                return $offer;
            }, $offers);

            $this->set('offers', $offers);

        }


    }