<?php
    namespace App\Controllers;

    class AdminuserManagementController extends \App\Core\Role\AdminRoleController {
        public function users(){
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $users = $userModel->getAll();
            $this->set('users', $users);
        }

        public function getEdit($userId){
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($userId);

            if(!$user){
                $this->redirect(\Configuration::BASE . 'admin/users');
            }

            $this->set('user', $user);

            return $userModel;
        }

        public function postEdit($userId){
            $userModel = $this->getEdit($userId);

            $username = \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $email = \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $password = \filter_input(INPUT_POST, 'password_hash', FILTER_SANITIZE_STRING);
            $forename = \filter_input(INPUT_POST, 'forename', FILTER_SANITIZE_STRING);
            $surname = \filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);
            $address = \filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            $phone = \filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

            $validanUsername = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($username);
            if ( !$validanUsername ) {
            $this->set('message','Doslo je do greske: Username nije ispravnog formata.');
            return;
            }

            $validanEmail = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($email);
            if ( !$validanEmail ) {
            $this->set('message','Doslo je do greske: Email nije ispravnog formata.');
            return;
            }
            
            $validanPassword = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($password);
            if ( !$validanPassword ) {
            $this->set('message','Doslo je do greske: Password nije ispravnog formata.');
            return;
            }

            $validanForename = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($forename);
            if ( !$validanForename ) {
            $this->set('message','Doslo je do greske: Forename nije ispravnog formata.');
            return;
            }

            $validanSurname = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($surname);
            if ( !$validanSurname ) {
            $this->set('message','Doslo je do greske: Surname nije ispravnog formata.');
            return;
            }

            $validanAddress = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64*1024)
                                ->isValid($address);
            if ( !$validanAddress ) {
            $this->set('message','Doslo je do greske: Address nije ispravnog formata.');
            return;
            }

            $validanPhone = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(24)
                                ->isValid($phone);
            if ( !$validanPhone ) {
            $this->set('message','Doslo je do greske: Phone nije ispravnog formata.');
            return;
            }

            $validanIsActive = (new \App\Validators\BitValidator())
            ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            } 

            $passwordHash = \password_hash($password, PASSWORD_DEFAULT);

            $userModel->editById($userId, [
                'username' => $username,
                'email' => $email,
                'password_hash' => $passwordHash,
                'forename' => $forename,
                'surname' => $surname,
                'address' => $address,
                'phone' => $phone,
                'is_active' => $isActive
            ]);

            $this->redirect(\Configuration::BASE . 'admin/users');
        }

        public function getAdd(){


        }

        public function postAdd(){
            $username = \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $email = \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $password = \filter_input(INPUT_POST, 'password_hash', FILTER_SANITIZE_STRING);
            $forename = \filter_input(INPUT_POST, 'forename', FILTER_SANITIZE_STRING);
            $surname = \filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);
            $address = \filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            $phone = \filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);
            
            $validanUsername = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($username);
            if ( !$validanUsername ) {
            $this->set('message','Doslo je do greske: Username nije ispravnog formata.');
            return;
            }

            $validanEmail = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($email);
            if ( !$validanEmail ) {
            $this->set('message','Doslo je do greske: Email nije ispravnog formata.');
            return;
            }
            
            $validanPassword = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($password);
            if ( !$validanPassword ) {
            $this->set('message','Doslo je do greske: Password nije ispravnog formata.');
            return;
            }

            $validanForename = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($forename);
            if ( !$validanForename ) {
            $this->set('message','Doslo je do greske: Forename nije ispravnog formata.');
            return;
            }

            $validanSurname = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64)
                                ->isValid($surname);
            if ( !$validanSurname ) {
            $this->set('message','Doslo je do greske: Surname nije ispravnog formata.');
            return;
            }

            $validanAddress = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(64*1024)
                                ->isValid($address);
            if ( !$validanAddress ) {
            $this->set('message','Doslo je do greske: Address nije ispravnog formata.');
            return;
            }

            $validanPhone = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(24)
                                ->isValid($phone);
            if ( !$validanPhone ) {
            $this->set('message','Doslo je do greske: Phone nije ispravnog formata.');
            return;
            }

            $validanIsActive = (new \App\Validators\BitValidator())
            ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            } 
                
                $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
                $user = $userModel->getByFieldName('email', $email);
                if($user){
                    $this->set('message', 'Doslo je do greske: vec postoji korisnik sa tom email adresom.');
                    return;
                }

                $user = $userModel->getByFieldName('username', $username);
                if($user){
                    $this->set('message', 'Doslo je do greske: vec postoji korisnik sa tim korisnickim imenom.');
                    return;
                }

                $passwordHash = \password_hash($password, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                'username' => $username,
                'email' => $email,
                'password_hash' => $passwordHash,
                'forename' => $forename,
                'surname' => $surname,
                'address' => $address,
                'phone' => $phone,
                'is_active' => $isActive
            ]);

            if($userId){
                $this->redirect(\Configuration::BASE . 'admin/users');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovog korisnika!');

        }

    }
