<?php
    namespace App\Controllers;

    class ImageController extends \App\Core\Controller{ 
        public function show($id) {

            $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());
            $image = $imageModel->getById($id);
            
            if(!$image){
                header('Location: /turbotech');
                exit;
            }
            //print_r($image);
            exit;

            $this->set('image', $image);

            $this->set('products', (new \App\Models\ProductModel($this->getDatabaseConnection()))->getAllByImageId($id));



            /*$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            echo($ipAddress);
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
            echo($userAgent);*/

           /* $auctionViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());
            $auctionViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]
                );*/

        }

        public function showAll() {
            $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());
            $images = $imageModel->getAll();
            $this->set('images', $images);

        }

    }