<?php
    namespace App\Controllers;

    class AdminProductManagementController extends \App\Core\Role\AdminRoleController {
        public function products(){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
        }

        public function getEdit($productId){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if(!$product){
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('product', $product);

            return $productModel;
        }

        public function postEdit($productId){
            $productModel = $this->getEdit($productId);

            $productNo = \filter_input(INPUT_POST, 'product_no', FILTER_SANITIZE_NUMBER_INT);
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT);
            $producer = \filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $size = \filter_input(INPUT_POST, 'size', FILTER_SANITIZE_NUMBER_FLOAT);
            $weight = \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

            $validanProductNo = (new \App\Validators\NumberValidator())
                                    ->setIntegerLength(11)
                                    ->isValid($productNo);

            if ( !$validanProductNo ) {
            $this->set('message','Doslo je do greske: ProductNo nije ispravnog formata.');
            return;
            }

            $validanTitle = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanDescription = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(64*1024)
                                    ->isValid($description);

            if ( !$validanDescription ) {
            $this->set('message','Doslo je do greske: Description nije ispravnog formata.');
            return;
            }

            $validanPrice = (new \App\Validators\NumberValidator())
                            ->setUnsigned()
                            ->setIntegerLength(10)
                            ->setMaxDecimalDigits(2)
                            ->isValid($price);

            if ( !$validanPrice ) {
            $this->set('message','Doslo je do greske: Price nije ispravnog formata.');
            return;
            }

            $validanProducer = (new \App\Validators\StringValidator())
                            ->setMinLength(1)
                            ->setMaxLength(255)
                            ->isValid($producer);

            if ( !$validanProducer ) {
            $this->set('message','Doslo je do greske: Producer nije ispravnog formata.');
            return;
            }

            $validanSize = (new \App\Validators\NumberValidator())
                            ->setUnsigned()
                            ->setIntegerLength(62)
                            ->setMaxDecimalDigits(2)
                            ->isValid($size);

            if ( !$validanSize ) {
            $this->set('message','Doslo je do greske: Size nije ispravnog formata.');
            return;
            }

            $validanWeight = (new \App\Validators\NumberValidator())
                            ->setUnsigned()
                            ->setIntegerLength(62)
                            ->setMaxDecimalDigits(2)
                            ->isValid($weight);

            if ( !$validanWeight ) {
            $this->set('message','Doslo je do greske: Size nije ispravnog formata.');
            return;
            }

            $validanIsActive = (new \App\Validators\BitValidator())
            ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            } 

            $productModel->editById($productId, [
                'product_no' => $productNo,
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'producer' => $producer,
                'size' => $size,
                'weight' => $weight,
                'is_active' => $isActive
            ]);

            if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
                $uploadStatus = $this->doImageUpload('image', $productId);
                if(!$uploadStatus) {
                    $this->set('message', 'Proizvod je izmenjen, ali nije dodata nova slika!');
                    return;
                }
            }

            $this->redirect(\Configuration::BASE . 'admin/products');
        }

        public function getAdd(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

        }

        public function postAdd(){
            $productNo = \filter_input(INPUT_POST, 'product_no', FILTER_SANITIZE_NUMBER_INT);
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT);
            $producer = \filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $size = \filter_input(INPUT_POST, 'size', FILTER_SANITIZE_NUMBER_FLOAT);
            $weight = \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());

            $validanProductNo = (new \App\Validators\NumberValidator())
            ->setIntegerLength(11)
            ->isValid($productNo);

            if ( !$validanProductNo ) {
            $this->set('message','Doslo je do greske: ProductNo nije ispravnog formata.');
            return;
            }

            $validanTitle = (new \App\Validators\StringValidator())
                    ->setMinLength(1)
                    ->setMaxLength(255)
                    ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanDescription = (new \App\Validators\StringValidator())
                        ->setMinLength(1)
                        ->setMaxLength(64*1024)
                        ->isValid($description);

            if ( !$validanDescription ) {
            $this->set('message','Doslo je do greske: Description nije ispravnog formata.');
            return;
            }

            $validanPrice = (new \App\Validators\NumberValidator())
                ->setUnsigned()
                ->setIntegerLength(10)
                ->setMaxDecimalDigits(2)
                ->isValid($price);

            if ( !$validanPrice ) {
            $this->set('message','Doslo je do greske: Price nije ispravnog formata.');
            return;
            }

            $validanProducer = (new \App\Validators\StringValidator())
                ->setMinLength(1)
                ->setMaxLength(255)
                ->isValid($producer);

            if ( !$validanProducer ) {
            $this->set('message','Doslo je do greske: Producer nije ispravnog formata.');
            return;
            }

            $validanSize = (new \App\Validators\NumberValidator())
                ->setUnsigned()
                ->setIntegerLength(62)
                ->setMaxDecimalDigits(2)
                ->isValid($size);

            if ( !$validanSize ) {
            $this->set('message','Doslo je do greske: Size nije ispravnog formata.');
            return;
            }

            $validanWeight = (new \App\Validators\NumberValidator())
                ->setUnsigned()
                ->setIntegerLength(62)
                ->setMaxDecimalDigits(2)
                ->isValid($weight);

            if ( !$validanWeight ) {
            $this->set('message','Doslo je do greske: Size nije ispravnog formata.');
            return;
            }

            $validanIsActive = (new \App\Validators\BitValidator())
            ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            } 

            $productId = $productModel->add([
                'product_no' => $productNo,
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'producer' => $producer,
                'size' => $size,
                'weight' => $weight,
                'is_active' => $isActive
            ]);

            if (!$productId) {
                $this->set('message', 'Došlo je do greške: Nije moguće dodati ovaj proizvod!');
            }

            $uploadStatus = $this->doImageUpload('image', $productId);
            if(!$uploadStatus) {
                $this->set('message', 'Proizvod je dodat, ali nije dodata nova slika!');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/products');
        }

        private function doImageUpload(string $fieldName, string $productId): bool{
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById(intval($productId));

           // unlink(\Configuration::UPLOAD_DIR . $product->image_path);
            array_map("unlink", glob(\Configuration::UPLOAD_DIR . $productId . ".*"));

            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($productId);
            $file->addValidations([
                new \Upload\Validation\Mimetype(["image/jpeg", "image/png"]),
                new \Upload\Validation\Size("3M")
            ]);

            try{
                $file->upload();

                $fullFileName = $file->getNameWithExtension();

                $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
                $productModel->editById(intval($productId), [
                    'image_path' => $fullFileName
                ]);

                $this->doResize(
                    \Configuration::UPLOAD_DIR . $fullFileName,
                    \Configuration::DEFAULT_IMAGE_WIDTH,
                    \Configuration::DEFAULT_IMAGE_HEIGHT
                );

                return true;
            }catch(\Exception $e){
                $this->set('message', 'Greska: ' . implode(', ', $file->getErrors()));
                return false;
            }

        }
        private function doResize(string $filePath, int $w, int $h){
            $longer = max($w, $h);

            $image = new \Gumlet\ImageResize($filePath);
            $image->resizeToBestFit($longer, $longer);
            $image->crop($w, $h);
            $image->save($filePath);
        }
    }