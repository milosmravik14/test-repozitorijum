<?php
    namespace App\Controllers;

    class UserProductManagementController extends \App\Core\Role\UserRoleController {
        public function products(){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
        }

        public function getEdit($productId){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if(!$product){
                $this->redirect(\Configuration::BASE . 'user/products');
            }

            $this->set('product', $product);

            return $productModel;
        }

        public function postEdit($productId){
            $productModel = $this->getEdit($productId);

            $productNo = \filter_input(INPUT_POST, 'product_no', FILTER_SANITIZE_NUMBER_INT);
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT);
            $producer = \filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $size = \filter_input(INPUT_POST, 'size', FILTER_SANITIZE_NUMBER_FLOAT);
            $weight = \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT);

            $productModel->editById($productId, [
                'product_no' => $productNo,
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'producer' => $producer,
                'size' => $size,
                'weight' => $weight
            ]);

            $this->redirect(\Configuration::BASE . 'user/products');
        }

        public function getAdd(){


        }

        public function postAdd(){
            $productNo = \filter_input(INPUT_POST, 'product_no', FILTER_SANITIZE_NUMBER_INT);
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT);
            $producer = \filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $size = \filter_input(INPUT_POST, 'size', FILTER_SANITIZE_NUMBER_FLOAT);
            $weight = \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());

            $productId = $productModel->add([
                'product_no' => $productNo,
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'producer' => $producer,
                'size' => $size,
                'weight' => $weight
            ]);

            if($productId){
                $this->redirect(\Configuration::BASE . 'user/products');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovaj proizvod!');

        }


    }