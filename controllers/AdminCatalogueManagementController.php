<?php
    namespace App\Controllers;

    class AdminCatalogueManagementController extends \App\Core\Role\AdminRoleController {
        public function catalogues() {
            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogues = $catalogueModel->getAll();
            $this->set('catalogues', $catalogues);
        }



        public function getEdit($catalogueId){
            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogue = $catalogueModel->getById($catalogueId);

            if(!$catalogue){
                $this->redirect(\Configuration::BASE . 'admin/catalogues');
            }

            $this->set('catalogue', $catalogue);

            return $catalogueModel;
        }

        public function postEdit($catalogueId){
            $catalogueModel = $this->getEdit($catalogueId);

            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

            $validanTitle = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(255)
                                    ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanDescription = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(64*1024)
                                    ->isValid($description);

            if ( !$validanDescription ) {
            $this->set('message','Doslo je do greske: Description nije ispravnog formata.');
            return;
            }

            $catalogueModel->editById($catalogueId, [
                'title' => $title,
                'description' => $description,
            ]);

            $this->redirect(\Configuration::BASE . 'admin/catalogues');
        }

        public function getAdd(){


        }

        public function postAdd(){
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());

            $validanTitle = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(255)
                                    ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanDescription = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(64*1024)
                                    ->isValid($description);

            if ( !$validanDescription ) {
            $this->set('message','Doslo je do greske: Description nije ispravnog formata.');
            return;
            }

            $catalogueId = $catalogueModel->add([
                'title' => $title,
                'description' => $description,
            ]);

            if($catalogueId){
                $this->redirect(\Configuration::BASE . 'admin/catalogues');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovaj katalog!');

        }
    }
