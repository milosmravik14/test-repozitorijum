<?php
    namespace App\Controllers;

    class ApiBookmarkController extends \App\Core\ApiController {
        public function getBookmarks(){
            $bookmarks = $this->getSession()->get('bookmarks', []);
            $this->set('bookmarks', $bookmarks);
        }


        public function addBookmarks($productId) {
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if(!$product){
                $this->set('error', -1);
                return;
            }

            $bookmarks = $this->getSession()->get('bookmarks', []);

            foreach ($bookmarks as $bookmark){
                if($bookmark->product_id == $productId){
                    $this->set('error', -2);
                    return; 
            }
        }

            $bookmarks[] = $product;
            $this->getSession()->put('bookmarks', $bookmarks);

            $this->set('error', 0);
            return;
        }

        public function clear(){
            $this->getSession()->put('bookmarks', []);

            $this->set('error', 0);

        }
    }