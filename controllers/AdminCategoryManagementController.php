<?php
    namespace App\Controllers;

    class AdminCategoryManagementController extends \App\Core\Role\AdminRoleController {
        public function categories() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getEdit($categoryId){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($categoryId);

            if(!$category){
                $this->redirect(\Configuration::BASE . 'admin/categories');
            }

            $this->set('category', $category);

            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogues = $catalogueModel->getAll();
            $this->set('catalogues', $catalogues);

            return $categoryModel;
        }

        public function postEdit($categoryId){
            $categoryModel = $this->getEdit($categoryId);

            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $imagePath = \filter_input(INPUT_POST, 'image_path', FILTER_SANITIZE_STRING);
            $catalogueId = \filter_input(INPUT_POST, 'catalogue_id', FILTER_SANITIZE_NUMBER_INT);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

            $validanTitle = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(255)
                                ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanImagePath = (new \App\Validators\StringValidator())
                                    ->setMinLength(1)
                                    ->setMaxLength(255)
                                    ->isValid($imagePath);

            if ( !$validanImagePath ) {
            $this->set('message','Doslo je do greske: Image path nije ispravnog formata.');
            return;
            }

            $validanCatalogueId = (new \App\Validators\NumberValidator())
                                    ->setIntegerLength(11)
                                    ->isValid($catalogueId);

            if ( !$validanCatalogueId ) {
            $this->set('message','Doslo je do greske: Catalogue ID nije ispravnog formata.');
            return;
            }
            
            $validanIsActive = (new \App\Validators\BitValidator())
                                    ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            }  

            $categoryModel->editById($categoryId, [
                'title' => $title,
                'image_path' => $imagePath,
                'catalogue_id' => $catalogueId,
                'is_active' => $isActive
            ]);

            $this->redirect(\Configuration::BASE . 'admin/categories');
        }

        public function getAdd(){
            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogues = $catalogueModel->getAll();
            $this->set('catalogues', $catalogues);

        }

        public function postAdd(){
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $imagePath = \filter_input(INPUT_POST, 'image_path', FILTER_SANITIZE_STRING);
            $catalogueId = \filter_input(INPUT_POST, 'catalogue_id', FILTER_SANITIZE_NUMBER_INT);
            $isActive = \filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());

            $validanTitle = (new \App\Validators\StringValidator())
            ->setMinLength(1)
            ->setMaxLength(255)
            ->isValid($title);

            if ( !$validanTitle ) {
            $this->set('message','Doslo je do greske: Title nije ispravnog formata.');
            return;
            }

            $validanImagePath = (new \App\Validators\StringValidator())
                            ->setMinLength(1)
                            ->setMaxLength(255)
                            ->isValid($imagePath);

            if ( !$validanImagePath ) {
            $this->set('message','Doslo je do greske: Image path nije ispravnog formata.');
            return;
            }

            $validanCatalogueId = (new \App\Validators\NumberValidator())
                            ->setIntegerLength(11)
                            ->isValid($catalogueId);

            if ( !$validanCatalogueId ) {
            $this->set('message','Doslo je do greske: Catalogue ID nije ispravnog formata.');
            return;
            }

            $validanIsActive = (new \App\Validators\BitValidator())
                            ->isValid($isActive);

            if ( !$validanIsActive ) {
            $this->set('message','Doslo je do greske: Is active nije ispravnog formata.');
            return;
            }  

            $categoryId = $categoryModel->add([
                'title' => $title,
                'image_path' => $imagePath,
                'catalogue_id' => $catalogueId,
                'is_active' => $isActive
            ]);

            if($categoryId){
                $this->redirect(\Configuration::BASE . 'admin/categories');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovu kategoriju!');

        }
    }