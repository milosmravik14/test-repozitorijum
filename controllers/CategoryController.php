<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller{   //grupisu celine na logican nacin, iz kontrolera ne smemo da pristupamo bazi podataka
        
        public function view() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);
                
            }

        public function show($id) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category){
                header('Location: /turbotech');
                exit;
            }

           /* $category = array_filter($category, function($cat) {
                return $cat->is_active;
            });*/


            $this->set('category', $category);
           
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $productsInCategory = $productModel->getAllByCategoryId($id);

            $productsInCategory = array_filter($productsInCategory, function($product) {
                return $product->is_active;
            });

            $this->set('productsInCategory', $productsInCategory);

        }

        
        public function delete($id) {
            die("Nije zavrsena implementacija brisanja!");

        }


    }