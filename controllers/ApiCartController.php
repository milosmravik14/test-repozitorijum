<?php
    namespace App\Controllers;

    class ApiCartController extends \App\Core\ApiController {
        public function showCart() {
            $carts = $this->getSession()->get('carts', []);
            $this->set('carts', $carts);
        }

        public function addToCart($productId) {
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if (!$product) {
                $this->set('error', -1);
                return;
            }

            $carts = $this->getSession()->get('carts', []);

            foreach ($carts as $cart) {
                if ($cart->product_id == $productId) {
                    $this->set('error', -2);
                    return;
                }
            }

            $carts[] = $part;
            $this->getSession()->put('carts', $carts);

            $this->set('error', 0);
        }

        public function clear() {
            $this->getSession()->put('carts', []);

            $this->set('error', 0);
        }
    }
