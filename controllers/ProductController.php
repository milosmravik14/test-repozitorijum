<?php
    namespace App\Controllers;

    class ProductController extends \App\Core\Controller{   //grupisu celine na logican nacin, iz kontrolera ne smemo da pristupamo bazi podataka
        
        public function show($id) {

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($id);
            
            if(!$product){
                header('Location: /turbotech');
                exit;
            }

            $this->set('product', $product);

           


            /*$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            echo($ipAddress);
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
            echo($userAgent);*/

           /* $productViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());
            $productViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]
                );*/

        }

        private function normaliseKeywords(string $keywords): string{
            $keywords = trim($keywords);
            $keywords = preg_replace('/ +/', ' ', $keywords);

            return $keywords;

        }

        public function searchProducts(){
           # $this->set('products', (new \App\Models\ProductModel($this->getDatabaseConnection()))->searchProducts($productTitle));

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = $this->normaliseKeywords($q);

            $products = $productModel->getAllBySearch($q);

            $this->set('products', $products);

        }

        public function postFilter(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
    
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $producer = filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT);
            $price_to = filter_input(INPUT_POST, 'price_to', FILTER_SANITIZE_NUMBER_FLOAT);
            $products = $productModel->getAllByFilter($categoryId, $producer, $price, $price_to);

            $this->set('products', $products);
        }


        public function addToCart(){

            
        }

        //TESTIRANJE ADD METODA

        /*public function delete($id) {

            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            echo($ipAddress);
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
            echo($userAgent);

            $auctionViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());
            $auctionViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]
                );
        }*/


    }