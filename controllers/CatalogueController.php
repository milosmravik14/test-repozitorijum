<?php
    namespace App\Controllers;
    

    class CatalogueController extends \App\Core\Controller{ 
        public function showProducts($id) {

            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogue = $catalogueModel->getById($id);
            
            if(!$catalogue){
                header('Location: /turbotech');
                exit;
            }

            $this->set('catalogue', $catalogue);

            $this->set('products', (new \App\Models\ProductModel($this->getDatabaseConnection()))->getAllByCatalogueId($id));



            /*$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            echo($ipAddress);
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
            echo($userAgent);*/

           /* $auctionViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());
            $auctionViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]
                );*/

        }

        public function showCatalogues() {
            $catalogueModel = new \App\Models\CatalogueModel($this->getDatabaseConnection());
            $catalogues = $catalogueModel->getAll();
            $this->set('catalogues', $catalogues);

        }

    }