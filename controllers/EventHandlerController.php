<?php
    namespace App\Controllers;

    class EventHandlerController extends \App\Core\Controller {
        public function handle(string $type){
            $host = \filter_input(INPUT_SERVER, 'HTTP_HOST');

            if($host !== 'localhost'){
                return;
            }

            $eventModel = new \App\Models\EventModel($this->getDataBaseConnection());
            $events = $eventModel->getAllByTypeAndStatus($type, 'Pending');

            if(!count($events)) {
                return;
            }

            if($type === 'email'){
                $this->handleEmails($events);
            }
        }

        private function handleEmails(array $events) {
            foreach($events as $event){
                $this->handleEmailEvent($event);
            }
        }

        private function handleEmailEvent($event){
            $eventModel = new \App\Models\EventModel($this->getDataBaseConnection());


            $emailEventHandler = new \App\EventHandlers\EmailEventHandler();
            $emailEventHandler->setData($event->data);
            
            $eventModel->editById($event->event_id, [
                'status' => 'Started'
            ]);

            $newStatus = $emailEventHandler->handle();

            $eventModel->editById($event->event_id, [
                'status' => $newStatus
            ]);
        }

    }