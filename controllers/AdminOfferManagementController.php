<?php
    namespace App\Controllers;

    class AdminOfferManagementController extends \App\Core\Role\AdminRoleController {
        public function offers() {
            $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
            $offers = $offerModel->getAll();

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $offers = array_map(function($offer) use ($userModel){
                $offer->user = $userModel->getById($offer->user_id);
                return $offer;
            }, $offers);

            $this->set('offers', $offers);
        }



        public function getEdit($offerId){
            $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
            $offer = $offerModel->getById($offerId);

            if(!$offer){
                $this->redirect(\Configuration::BASE . 'admin/offers');
            }

            $this->set('offer', $offer);

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $users = $userModel->getAll();
            $this->set('users', $users);

            
            $currencyModel = new \App\Models\CurrencyModel($this->getDatabaseConnection());
            $currencies = $currencyModel->getAll();
            $this->set('currencies', $currencies);

            $shippingModel = new \App\Models\ShippingModel($this->getDatabaseConnection());
            $shippings = $shippingModel->getAll();
            $this->set('shippings', $shippings);

            return $offerModel;
        }

        public function postEdit($offerId){
            $offerModel = $this->getEdit($offerId);

            $status = \filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            $userId = \filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_NUMBER_INT);
            $currencyId = \filter_input(INPUT_POST, 'currency_id', FILTER_SANITIZE_NUMBER_INT);
            $shippingId = \filter_input(INPUT_POST, 'shipping_id', FILTER_SANITIZE_NUMBER_INT);

            $validanStatus = (new \App\Validators\StringValidator())
                                ->setMinLength(1)
                                ->setMaxLength(8)
                                ->isValid($status);

            if ( !$validanStatus ) {
            $this->set('message','Doslo je do greske: Status nije ispravnog formata.');
            return;
            }

            $validanUserId = (new \App\Validators\NumberValidator())
                                ->setIntegerLength(11)
                                ->isValid($userId);

            if ( !$validanUserId ) {
            $this->set('message','Doslo je do greske: User ID nije ispravnog formata.');
            return;
            }

            $validanCurrencyId = (new \App\Validators\NumberValidator())
                                    ->setIntegerLength(11)
                                    ->isValid($currencyId);
            
            if ( !$validanCurrencyId ) {
            $this->set('message','Doslo je do greske: Currency ID nije ispravnog formata.');
            return;
            }

            $validanShippingId = (new \App\Validators\NumberValidator())
                                    ->setIntegerLength(11)
                                    ->isValid($shippingId);

            if ( !$validanShippingId ) {
            $this->set('message','Doslo je do greske: Shipping ID nije ispravnog formata.');
            return;
            }

            $offerModel->editById($offerId, [
                'status' => $status,
                'user_id' => $userId,
                'currency_id' => $currencyId,
                'shipping_id' => $shippingId
            ]);

            $this->redirect(\Configuration::BASE . 'admin/offers');
        }

        public function getAdd(){
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $users = $userModel->getAll();
            $this->set('users', $users);

            $currencyModel = new \App\Models\CurrencyModel($this->getDatabaseConnection());
            $currencies = $currencyModel->getAll();
            $this->set('currencies', $currencies);

            $shippingModel = new \App\Models\ShippingModel($this->getDatabaseConnection());
            $shippings = $shippingModel->getAll();
            $this->set('shippings', $shippings);
        }

        public function postAdd(){
            $status = \filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            $userId = \filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_NUMBER_INT);
            $currencyId = \filter_input(INPUT_POST, 'currency_id', FILTER_SANITIZE_NUMBER_INT);
            $shippingId = \filter_input(INPUT_POST, 'shipping_id', FILTER_SANITIZE_NUMBER_INT);

            $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());

            $validanStatus = (new \App\Validators\StringValidator())
                        ->setMinLength(1)
                        ->setMaxLength(8)
                        ->isValid($status);

            if ( !$validanStatus ) {
            $this->set('message','Doslo je do greske: Status nije ispravnog formata.');
            return;
            }

            $validanUserId = (new \App\Validators\NumberValidator())
                        ->setIntegerLength(11)
                        ->isValid($userId);

            if ( !$validanUserId ) {
            $this->set('message','Doslo je do greske: User ID nije ispravnog formata.');
            return;
            }

            $validanCurrencyId = (new \App\Validators\NumberValidator())
                            ->setIntegerLength(11)
                            ->isValid($currencyId);

            if ( !$validanCurrencyId ) {
            $this->set('message','Doslo je do greske: Currency ID nije ispravnog formata.');
            return;
            }

            $validanShippingId = (new \App\Validators\NumberValidator())
                            ->setIntegerLength(11)
                            ->isValid($shippingId);

            if ( !$validanShippingId ) {
            $this->set('message','Doslo je do greske: Shipping ID nije ispravnog formata.');
            return;
            }

            $offerId = $offerModel->add([
                'status' => $status,
                'user_id' => $userId,
                'currency_id' => $currencyId,
                'shipping_id' => $shippingId
            ]);

            if($offerId){
                $this->redirect(\Configuration::BASE . 'admin/offers');
            }
            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovu porudžbinu!');
        }
    }
