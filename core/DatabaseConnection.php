<?php
    namespace App\Core;

    class DatabaseConnection {   //SINGLETONE PATERN (2 VIDEO, 6MIN)
        private $connection;
        private $configuration;
     

        public function __construct(DatabaseConfiguration $databaseConfiguration){
            $this->configuration = $databaseConfiguration;
        }

        public function getConnection(): \PDO {            //:PDO  \PDO - zbog namespace-a
            if($this->connection === NULL){
              $this->connection = new \PDO($this->configuration->getSourceString(), 
                                           $this->configuration->getUser(), 
                                           $this->configuration->getPass());

            }

            return $this->connection;      //SAMO CE PRVI PUT DA USPOSTAVI VREDNOST, SVAKI SLEDECI PUT SAMO SE OVA LINIJA KODA IZVRSAVA
        }

    }