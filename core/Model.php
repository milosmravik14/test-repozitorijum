<?php
    namespace App\Core;

    abstract class Model {    //mora biti nasledjena od strane druge klase
        private $dbc;

        final public function __construct(DatabaseConnection &$dbc){   //& da bude prosledjena po referenci a ne po kopiji
            $this->dbc = $dbc;
        }

        protected function getFields(): array {
            return [ ];
        }

        
        final protected function getDatabaseConnection(){
            return $this->dbc;

        }

        final protected function getConnection(){
            return $this->dbc->getConnection();

        }

        final protected function getTableName(): string {
            $matches = [];
            preg_match('|^.*\\\((?:[A-Z][a-z]+)+)Model$|', static::class, $matches);
            return substr(strtolower(preg_replace('|[A-Z]|', '_$0',  $matches[1])), 1);
        }


        final public function getById(int $id) {
            $table = $this->getTableName();
            $sql = "SELECT * FROM {$table} WHERE {$table}_id = ?;";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute( [ $id ] );

            $item = NULL;
            if ( $res ) {
                $itemObject = $prep->fetch(\PDO::FETCH_OBJ);
                if ($itemObject !== FALSE) {
                    $item = $itemObject;
                }
            }

            return $item;
        }

        final public function getAll(): array {
            $table = $this->getTableName();
            $sql = "SELECT * FROM {$table};";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute();

            $items = [];
            if ( $res ) {
                $items = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $items;
        }

        final public function isFieldValueValid(string $fieldName, $fieldValue): bool {
            $fields = $this->getFields();
            $supportedFieldNames = array_keys($fields);

            if(!in_array($fieldName, $supportedFieldNames)){
                return false;
            }

            return $fields[$fieldName]->isValid($fieldValue);
        }

        public function getByFieldName(string $fieldName, $value){
            if(!$this->isFieldValueValid($fieldName, $value)){
                throw new \Exception('Invalid field name or value:' . $fieldName);
            }

            $tableName = $this->getTableName();
            $sql = 'SELECT * FROM ' . $tableName . '  WHERE '. $fieldName.' = ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$value]);
            
            $item = NULL;
            if($res){
                $item = $prep->fetch(\PDO::FETCH_OBJ);

            }
            return $item;
        }

        final public function getAllByFieldName(string $fieldName, $value): array{
            if(!$this->isFieldValueValid($fieldName, $value)){
                throw new Exception('Invalid field name or value:' . $fieldName);
            }
            $tableName = $this->getTableName();
            $sql = 'SELECT * FROM ' . $tableName . '  WHERE '. $fieldName.' = ?;';
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$value]);
            
            $items = [];
            if($res){
                $items = $prep->fetchAll(\PDO::FETCH_OBJ);

            }
            return $items;
        }

        final private function checkDataValues(array &$data) {
            $trazenKljucevi = array_keys($data);

            $validators = $this->getFields();
            $fieldNameList = array_keys($validators);

            foreach ($trazenKljucevi as $kljuc) {
                if (!in_array($kljuc, $fieldNameList)) {
                    throw new \Exception('Polje ' . $kljuc . ' ne postoji u ovoj tabeli!');
                }

                if (!$validators[$kljuc]->isEditable()) {
                    throw new \Exception('Polje ' . $kljuc . ' ne mozete rucno da definisete!');
                }

                if (!$validators[$kljuc]->isValid($data[$kljuc])) {
                    throw new \Exception('Vrednost za kljuc ' . $kljuc . ' ne odgovara njenom sablonu!');
                }
            }
        }
        

        final public function add(array $data) {
            $this->checkDataValues($data);
            $fields = $this->getFields();

            $trazenKljucevi = array_keys($data);

            $table = $this->getTableName();

            $spisakPolja = implode(', ', $trazenKljucevi);

            $spisakUpitnika = str_repeat('?,', count($trazenKljucevi));
            $spisakUpitnika = substr($spisakUpitnika, 0, -1);

            $sql = "INSERT INTO {$table} ($spisakPolja) VALUES ({$spisakUpitnika});";

            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute(array_values($data));

            if (!$res) {
                return false;
            }

            return $this->dbc->getConnection()->lastInsertId();
        }


        final public function editById($id, array $data) {
            $this->checkDataValues($data);

            $table = $this->getTableName();

            $updateFieldList = [];
            $values = [];
            foreach ($data as $polje => $vrednost) {
                $updateFieldList[] = $polje . ' = ?';
                $values[] = $vrednost;
            }
            $updateFields = implode(', ', $updateFieldList);
            $values[] = $id;

            $sql = "UPDATE {$table} SET {$updateFields} WHERE {$table}_id = ?;";
            $prep = $this->dbc->getConnection()->prepare($sql);
            return $prep->execute($values);
        }

        final public function deleteById($id) {
            $table = $this->getTableName();
            $sql = "DELETE FROM {$table} WHERE {$table}_id = ?;";
            $prep = $this->dbc->getConnection()->prepare($sql);
            return $prep->execute( [ $id ] );
        }
    }